# lookup
Double click on a word to display its 
English definition on a dedicated notification. 
Hold you mouse pointer on the notification to keep displaying the definition.

* works offline

## WARNING: 
Tested on Plasma and Gnome desktop environments. On plasma lookup works like a
charm yet on Gnome definitons results in cropped text sometimes.
### Requirements

* [GNU Bash](https://www.gnu.org/software/bash/)
* [dictd](https://sourceforge.net/projects/dict/)
* [libnotify](https://developer.gnome.org/notification-spec/)
* [xclip](https://sourceforge.net/projects/xclip/)
* [gcide](http://gcide.gnu.org.ua/) or [wordnet](https://wordnet.princeton.edu/) dictionary databases

To install all dependencies on ubuntu run `apt install bash dictd libnotify dict-{gcide,wn}  xclip` as root

### Installation
1. git clone this repo to your computer. like: ```sh
git clone https://gitlab.com/TheCodeRunsMe/lookup.git```
2. run `lookup`, possibly as a startup script
